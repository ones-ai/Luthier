from utils import *
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', "--pickles", nargs="+", required=True, help="list of pickle file.")
    parser.add_argument('-o', "--output", type=str, required=True, help="output.")
    parser.add_argument('-t', "--title", type=str, required=True, help="title of figure.")
    args = parser.parse_args()

    combine_draw(args.pickles, args.title, args.output)
