#!/bin/bash

sudo docker build -t registry.gitlab.com/ones-ai/luthier .
sudo docker run --rm -it -v ./output:/app/output registry.gitlab.com/ones-ai/luthier /bin/bash
