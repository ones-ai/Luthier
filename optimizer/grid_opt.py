from .opt import *
import numpy as np
import logging
logging.basicConfig(format='%(levelname)s : %(message)s ', level=logging.DEBUG)

class GridOptimizer(Optimizer):
    def name(self):
        return "Grid"

    def init(self, file):
        super().init(file=file)

    def result(self):
        return {'t': np.zeros(len(self.data)), 'x': np.arange(1, len(self.data) + 1), 'y': self.real_result}

    def next(self):
        if self.index >= len(self.real_result):
            return None

        return self.real_result[self.index]

    def train(self, iterate: int, seed: int):
        self.index = 0
        self.data = self.model_data
        # self.data = self.model_data.sample(frac=1.0, random_state=seed)
        self.result_opt = []
        iter = 0
        step_size = int(len(self.data) / iterate)
        for idx, row in enumerate(self.data.itertuples()):
            if idx % step_size == 0:
                iter += 1
                logging.info('[{0}] iteration : {1}, index: {2}, inference time : {3} ms, info: {4}'.format(self.name(), iter, row.index, row.kernel_compute_time/1e6, str(row.info)))
                self.result_opt.append((row.kernel_compute_time / 1e6))
        self.real_result = np.minimum.accumulate(self.result_opt)
