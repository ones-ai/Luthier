from sklearn.model_selection import GridSearchCV
from xgboost import XGBRegressor

def grid_search(data_X, data_Y, num): # xgboost의 최적의 하이퍼 파라미터를 찾는 알고리즘 (학습 데이터, 학습 레이블 데이터, 찾고자하는 파라미터 번호)
    xgb = XGBRegressor()

    if num == 1:
        param_test = { # 각 param_test를 하나씩 실행하면서 최적의 파라미터를 찾으면 됨
        'max_depth':range(1,10),
        'min_child_weight':range(2,10)
        }
    elif num == 2:
        param_test = {
        'gamma':[i/10.0 for i in range(0,10)]
        }
    elif num == 3:
        param_test = {
        'subsample':[i/10.0 for i in range(2,15)],
        'colsample_bytree':[i/10.0 for i in range(2,15)]
        }
    elif num == 4:
        param_test = {
        'subsample':[i/100.0 for i in range(40,100)],
        }
    elif num == 5:
        param_test = {
        'reg_alpha':[1e-5, 1e-2, 0.1, 1, 10, 100]
        }
    elif num == 6:
        param_test = {
            'learning_rate': [0.2, 0.1, 0.02, 0.05, 0.01, 0.001]
        }
    else:
        print("파라미터 범위를 벗어난 값을 입력하였습니다.")

    params = {'objective': 'reg:squarederror',
            'learning_rate':0.2,
            'max_depth':9,
            'min_child_weight': 2,
            'gamma':0.0,
            'reg_alpha':1,
            'subsample':0.53,
            'colsample_bytree':0.8}

    gsearch1 = GridSearchCV(xgb, param_grid = param_test, scoring='neg_mean_absolute_error', n_jobs=-1, cv=5, verbose=10)

    gsearch1.fit(data_X.to_numpy(), data_Y.to_numpy())
    print()
    print(gsearch1.cv_results_)
    print()
    print(gsearch1.best_params_)
    print()
    print(gsearch1.best_score_)
