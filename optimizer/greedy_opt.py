from .opt import *
import numpy as np
import logging
logging.basicConfig(format='%(levelname)s : %(message)s ', level=logging.DEBUG)

class GreedyOptimizer(Optimizer):
    def name(self):
        return "Greedy"

    def init(self, file):
        super().init(file=file)
        self.scale = 40

    def result(self):
        return {'t': np.zeros(len(self.data)), 'x': np.arange(1, len(self.data) + 1), 'y': self.real_result}

    def next(self):
        if self.index >= len(self.real_result):
            return None

        return self.real_result[self.index]

    def compute_time(self, x):
        return self.data.iloc[x].kernel_compute_time /1e6

    def local_search(self, best_x, neighborhood_size, max_len):
        local_best_x = best_x
        local_best_f = self.compute_time(local_best_x)
        for i in range(max(0, best_x - neighborhood_size), min(max_len, best_x + neighborhood_size) + 1):
            if i not in self.computed_times:
                f = self.compute_time(i)
                self.result_opt.append(f)
                self.computed_times[i] = f
                if f < local_best_f:
                    local_best_f = f
                    local_best_x = i
        return local_best_x

    def train(self, iterate: int, seed: int):
        np.random.seed(seed)
        self.data = self.model_data
        data_len = len(self.data)
        self.result_opt = []
        self.computed_times = {}
        rows = []
        for row in self.data.itertuples():
            rows.append(row)

        iter = 0
        step = max(int(iterate/self.scale),1)
        prev_best_x = np.random.randint(0, data_len)
        for _ in range(0, iterate):
            best_x = self.local_search(prev_best_x, int(step), data_len)
            if best_x == prev_best_x:
                prev_best_x = np.random.randint(0, data_len)
            else:
                prev_best_x = best_x

            if len(self.result_opt) >= iterate:
                break;
            row = rows[best_x]
            iter += 1
            logging.info('[{0}] iteration : {1}, index: {2}, inference time : {3} ms, info: {4}'.format(self.name(), iter, row.index, row.kernel_compute_time/1e6, str(row.info)))
        self.real_result = np.minimum.accumulate(self.result_opt)
