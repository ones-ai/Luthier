import xgboost as xgb
import pandas as pd
import numpy as np
from bayes_opt import BayesianOptimization
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import r2_score, mean_squared_log_error, mean_squared_error
from sklearn.preprocessing import MinMaxScaler, RobustScaler, StandardScaler, MaxAbsScaler, Normalizer
import random
import time

from .opt import *

import logging
logging.basicConfig(format='%(levelname)s : %(message)s ', level=logging.DEBUG)

class BayesianOptimizer(Optimizer):
    def name(self):
        return "Bayesian"
    
    iter = 0
    def black_box_function(self, row): # , cpu1, cpu2, cpu3, cpu4, cpu5
        """Function with unknown internals we wish to maximize.

        This is just serving as an example, for all intents and
        purposes think of the internals of this function, i.e.: the process
        which generates its output values, as unknown.
        """
        start = time.time()
        self.iter += 1
        row = int(round(row))
        # cpu1 = int(round(cpu1))
        # cpu2 = int(round(cpu2))
        # cpu3 = int(round(cpu3))
        # cpu4 = int(round(cpu4))
        # cpu5 = int(round(cpu5))
        
        # filtered_rows = model1_X[(model1_X['cpu0'] == cpu0) & (model1_X['cpu1'] == cpu1) & (model1_X['cpu2'] == cpu2) & (model1_X['cpu3'] == cpu3) & (model1_X['cpu4'] == cpu4) & (model1_X['cpu5'] == cpu5)]
        # 만약 해당하는 행이 존재한다면 'latency' 값을 추출
        # if not filtered_rows.empty:
        #     indices = filtered_rows.index.tolist()
        #     y_values = model1_Y.loc[indices, 'score'].tolist()
            
        self.total_t += (time.time() - start) + (self.model1_Y.loc[row, 'kernel_compute_time']/1e6)
        self.list_time.append(self.total_t)
        #     return -float(y_values[0]/1e6)
        # else:
        #     return -900000000
        logging.info('[{0}] iteration : {1}, index: {2}, inference time : {3} ms, info: {4}'.format(self.name(), 
                                                                                                    self.iter, 
                                                                                                    row, 
                                                                                                    self.model1_Y.loc[row, 'kernel_compute_time']/1e6,
                                                                                                    str(self.model_data.loc[row, 'info'])))
        return -float(self.model1_Y .loc[row, 'kernel_compute_time']/1e6)
    
    def next(self):
        self.y_list = []
        self.x_list = []
        self.y_min = 90000000000.0
        
        if self.index_nex >= len(self.result_opt):
            return None
        
        i, res = self.result_opt[self.index_nex]

        print("Iteration {}: \n\t{}".format(i, res))
        
        if -900000000.0 != res.get("target"):
            self.x_list.append(i)
            if self.y_min > -res.get("target"):
                self.y_min = -res.get("target")
            self.y_list.append(self.y_min)
            
        self.index_nex += 1
        return -res.get("target")
        
    def result(self):
        self.y_list = []
        self.x_list = []
        self.y_min = 90000000000.0
        
        for i, res in self.result_opt:
            # print("Iteration {}: \n\t{}".format(i, res))
            
            if -900000000.0 != res.get("target"):
                self.x_list.append(i)
                if self.y_min > -res.get("target"):
                    self.y_min = -res.get("target")
                self.y_list.append(self.y_min)
                
        return {'x': self.x_list, 'y': self.y_list, 't': self.list_time}

    def train(self, iterate: int, seed: int):
        self.iter = 0
        # Bounded region of parameter space
        range_pbount = len(self.model_data) - 1
        self.index_nex = 0
        pbounds = {'row': (0, range_pbount)} #, 'cpu1': (0, 288), 'cpu2': (0, 288), 'cpu3': (0, 144), 'cpu4': (0, 144), 'cpu5': (0, 144)}

        # print(pbounds)

        optimizer = BayesianOptimization(
            f=self.black_box_function,
            pbounds=pbounds,
            random_state=seed,
            verbose=0
        )

        optimizer.maximize(
            init_points=30,
            n_iter=(iterate - 30),
        )

        # print(optimizer.max)
        self.result_opt = list(enumerate(optimizer.res))
        
    
    def init(self, file: pd.DataFrame):
        super().init(file)
        # read csv
        self.model1_X = self.model_data[["total_worksize","kernel_compute_time","cluster0_worksize","cluster1_worksize", 'conv_alg', 'conv_simd']]
        self.model1_Y = self.model_data[["kernel_compute_time"]]

        self.data_X = self.model1_X
        self.data_Y = self.model1_Y
        shuffle_index_list = list(range(len(self.data_X)))
        random.shuffle(shuffle_index_list)
            
        self.explode_X = self.data_X.loc[shuffle_index_list[:10], :]
        self.explode_Y = self.data_Y.loc[shuffle_index_list[:10], :]
        self.unexplode_X = self.data_X.drop(shuffle_index_list[:10], axis=0)
        self.unexplode_Y = self.data_Y.drop(shuffle_index_list[:10], axis=0)
        self.explode_X = self.explode_X.reset_index(drop=True)
        self.explode_Y = self.explode_Y.reset_index(drop=True)
        self.unexplode_X = self.unexplode_X.reset_index(drop=True)
        self.unexplode_Y = self.unexplode_Y.reset_index(drop=True)

        self.total_t = 0
        self.list_time = []
     