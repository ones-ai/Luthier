import pandas as pd
import numpy as np
import xgboost as xgb
# import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error
from random import *
import random
import time
import warnings
warnings.filterwarnings("ignore")

from .opt import *
import logging
logging.basicConfig(format='%(levelname)s : %(message)s ', level=logging.DEBUG)

class XGBoostOptimizer(Optimizer):
    model_data: pd.DataFrame
    def name(self):
        return "XGBoost"
    
    def __train(self, data_X, data_Y, iter: int, seeds: int, data_num: int = 5): # 학습을 하여 결과를 내는 함수 (학습 데이터, 학습 레이블 데이터, 저장하고자하는 모델의 이름)
        list_rmse = []
        list_min = []
        list_min_update = []
        list_min_num = []

        count = 1

        shuffle_index_list = list(range(len(data_Y)))
        random.seed(seeds)
        random.shuffle(shuffle_index_list)
            
        explode_X = data_X.loc[shuffle_index_list[:data_num], :]
        explode_Y = data_Y.loc[shuffle_index_list[:data_num], :]
        unexplode_X = data_X.drop(shuffle_index_list[:data_num], axis=0)
        unexplode_Y = data_Y.drop(shuffle_index_list[:data_num], axis=0)
        explode_X = explode_X.reset_index(drop=True)
        explode_Y = explode_Y.reset_index(drop=True)
        unexplode_X = unexplode_X.reset_index(drop=True)
        unexplode_Y = unexplode_Y.reset_index(drop=True)
            
        params = {'objective': 'reg:squarederror',
                'learning_rate':0.2,
                'max_depth':9,
                'min_child_weight': 2,
                'gamma':0.0,
                'reg_alpha':1,
                'subsample':0.53,
                'colsample_bytree':0.8,
                'eval_metric': 'rmse',
                'random_state': seeds
        }
            
        dtrain = xgb.DMatrix(explode_X.to_numpy(), label=explode_Y.to_numpy())
        dtest = xgb.DMatrix(unexplode_X.to_numpy(), label=unexplode_Y.to_numpy())
            
        model = xgb.train(params, dtrain, 100)
        self.each_time = []    

        for i in range (data_num, (iter + data_num), data_num): 
            start = time.time()
            model = xgb.train(params, dtrain, 100, xgb_model=model)
            
            pred = model.predict(dtest) # 학습에 사용하지 않은 데이터로 학습결과를 예측
            pred_rmse = mean_squared_error(pred, unexplode_Y.to_numpy(), squared=False)
            list_rmse.append(pred_rmse)
            
            pred_min_index = np.argmin(pred)
            pred_min = unexplode_Y.to_numpy()[pred_min_index]
            list_min.append(pred_min)
            
            min_num = np.where(pred == np.min(pred))[0].tolist()
            list_min_num.append(len(min_num))
            
            logging.info('[{0}] iteration : {1}, index: {2}, inference time : {3} ms, info: {4}'.format(self.name(), 
                                                                                                        i, 
                                                                                                        pred_min_index, 
                                                                                                        pred_min[0] / 1e6,
                                                                                                        str(self.model_data.loc[pred_min_index, 'info'])))
            
            if (len(list_min_update) == 0):
                list_min_update.append(pred_min)
            else :
                if  list_min_update[-1] > pred_min :
                    list_min_update.append(pred_min)
                else:
                    list_min_update.append(list_min_update[-1])
                    
            if (i < 50): # random update
                explode_X = data_X.loc[shuffle_index_list[:data_num*count], :]
                explode_Y = data_Y.loc[shuffle_index_list[:data_num*count], :]
                unexplode_X = data_X.drop(shuffle_index_list[:data_num*count], axis=0)
                unexplode_Y = data_Y.drop(shuffle_index_list[:data_num*count], axis=0)
                explode_X = explode_X.reset_index(drop=True)
                explode_Y = explode_Y.reset_index(drop=True)
                unexplode_X = unexplode_X.reset_index(drop=True)
                unexplode_Y = unexplode_Y.reset_index(drop=True)
                count = count + 1
            else : # 예측한 결과를 사용하여 다음 데이터 학습
                # 예측 결과로 업데이트
                pred_index = np.argpartition(pred, data_num-2)[:data_num-2]
                explode_X = pd.concat([explode_X, unexplode_X.loc[pred_index, :]], ignore_index=True) # np.append(explode_X, unexplode_X[pred_index], axis=0)
                explode_Y = pd.concat([explode_Y, unexplode_Y.loc[pred_index, :]], ignore_index=True)
                unexplode_X.drop(pred_index, axis=0, inplace=True)
                unexplode_Y.drop(pred_index, axis=0, inplace=True)
                
                explode_X = explode_X.reset_index(drop=True)
                explode_Y = explode_Y.reset_index(drop=True)
                unexplode_X = unexplode_X.reset_index(drop=True)
                unexplode_Y = unexplode_Y.reset_index(drop=True)
                
                # 랜덤 업데이트
                shuffle_list = list(range(len(unexplode_Y)))
                random.shuffle(shuffle_list)
                explode_X = pd.concat([explode_X, unexplode_X.loc[shuffle_list[:2], :]], ignore_index=True) # np.append(explode_X, unexplode_X[pred_index], axis=0)
                explode_Y = pd.concat([explode_Y, unexplode_Y.loc[shuffle_list[:2], :]], ignore_index=True)
                unexplode_X.drop(shuffle_list[:2], axis=0, inplace=True)
                unexplode_Y.drop(shuffle_list[:2], axis=0, inplace=True)
                
                explode_X = explode_X.reset_index(drop=True)
                explode_Y = explode_Y.reset_index(drop=True)
                unexplode_X = unexplode_X.reset_index(drop=True)
                unexplode_Y = unexplode_Y.reset_index(drop=True)

            dtrain = xgb.DMatrix(explode_X.to_numpy(), label=explode_Y.to_numpy()) # 데이터 업데이트
            dtest = xgb.DMatrix(unexplode_X.to_numpy(), label=unexplode_Y.to_numpy())
            
            exe_t = time.time() - start
            self.each_time.append(exe_t)
            # print("time :", exe_t)


        self.y1 = list_rmse # 매 학습시의 rmse 결과
        self.y2 = list_min # 예측 결과의 최소 latency
        self.y3 = list_min_update
        self.y4 = list_min_num # 최소 latency로 예측되는 데이터의 수

    def init(self, file):
        super().init(file)

    def result(self):
        total_duration = self.end_time - self.start_time
        t = np.repeat(self.each_time, self.num)
        rmse = np.repeat(self.y1, self.num)
        min = np.repeat(self.y2, self.num)
        min_update = np.repeat(self.y3, self.num)
        min_num = np.repeat(self.y4, self.num)

        return {'t': t, 'x': np.arange(1, len(t) + 1), 'y': np.minimum.accumulate(min / 1e6)
                , 'rmse': rmse, 'min_num': min_num, 'min_update': min_update
                }
    
    def train(self, iterate: int, seed: int):
        self.index_nex = 0
        self.loop_index = 0
        df_X = self.model_data[["total_worksize","cluster0_worksize","cluster1_worksize", 'conv_alg', 'conv_simd']]
        df_Y = self.model_data[["kernel_compute_time"]]

        self.start_time = time.time()
        self.num = 5
        size = len(self.model_data)
        max_search_range = min(size - (size % self.num), iterate)

        self.__train(df_X, df_Y, max_search_range, seed, self.num)
        self.end_time = time.time()

    def next(self):
        if self.loop_index < self.num:
            self.loop_index += 1
        else:
            self.index_nex += 1
            self.loop_index = 0

        if self.index_nex >= len(self.y2):
            return None
        
        value = self.y2[self.index_nex]
        return value
