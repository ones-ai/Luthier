import os
import numpy as np
import pandas as pd
from .common import Module
from .utils import *

class XNNPackModule(Module):
    def convert_layer_num(self, dir: str) -> np.array:
        l = os.listdir(dir)
        l = list(map(lambda x: x.split('_')[0], l))
        l = list(map(lambda x: int(x), l))
        l = list(set(l))
        l.sort()
        return l

    def __convert_per_layer(self, file) -> pd.DataFrame:
        f = pd.read_csv(file)
        f = f[['gemm_id','max_work_size','avg_kernel_time','cpu0','cpu1','cpu2','cpu3','cpu4','cpu5']]
        f = f.rename(columns={
            "max_work_size": "total_worksize",
            'avg_kernel_time': 'kernel_compute_time',
            }, inplace=False)
        
        f['cluster0_worksize'] = f['cpu0'] + f['cpu1']
        f['cluster1_worksize'] = f['cpu2'] + f['cpu3'] + f['cpu4'] + f['cpu5']

        f = f[['gemm_id', 'total_worksize', 'kernel_compute_time', 'cluster0_worksize', 'cluster1_worksize']]
        f = f.sort_values(by='cluster0_worksize').reset_index()
        f['info'] = f.apply(create_info_for_xnnpack(file), axis=1)
        f = f.drop_duplicates(subset=['cluster0_worksize', 'cluster1_worksize'])
        f['conv_alg'] = f['gemm_id']
        f['conv_simd'] = 0
        
        return f
        

    def validate_layer(self, dir_list: [str]) -> bool:
        assert(len(dir_list) != 0)

    
    def getlist(self, log_dir: str) -> [str]:
        l = os.listdir(log_dir)
        return l
    
    #  -> map[int, map[str, pd.DataFrame]]
    def convert(self, dir: str):
        d = self.getlist(dir)
        assert(len(d) != 0)

        self.validate_layer(d)
            
        result = {}
        for item in d:
            idx = int(item.split("_")[0])
            file = os.path.join(dir, item)
            result[idx] = {"opt": [self.__convert_per_layer(file)]}
            
        return result        