import json

def load_file(path, file: str):
    print(path + file)
    
    with open(path + file, 'r') as f:
        return json.load(f)

def find_name(target: [str], val: str) -> [str]:
    result = []
    # print(target)
    for i in target:
        if val in i:
            result.append(i)
    return result

def create_info_for_acl(row):
    return  {'little': row["cluster0_worksize"], 
             'big': row["cluster1_worksize"],
             'layer_id': row['layer_id'],
             'layer_name': row['layer_name'],
             'kernel_name': row['kernel_name'],
             'conv_alg': conv_method(row['conv_alg']),
             'conv_simd': conv_simd(row['conv_alg']),
    }

def create_info_for_xnnpack(file):
    # the latest index
    data = file.split('/')[-1]
    data = data.split('_')
    id = data[0]
    kernel_name = data[1]
    layer_name = data[2]
    
    def create_func(row):
        return {
            'little': row["cluster0_worksize"], 
            'big': row["cluster1_worksize"],
            'layer_id': id,
            'layer_name': layer_name,
            'kernel_name': kernel_name,
            'conv_alg': row['gemm_id'],
        }
    
    return create_func
    
def conv_method(val: int):
    if val == 0:
        return 'none'
    if val == 1:
        return "direct"
    if val == 2:
        return "im2col"
    if val == 3:
        return "winograd"
    
def conv_simd(val: int):
    simds = {
        0: 'none',
        1: 'a64_hybrid_fp32_mla_4x24', 
        2: 'a64_hybrid_fp32_mla_6x16', 
        3: 'a64_hybrid_fp32_mla_8x4', 
        4: 'a64_sgemm_8x6', 
        5: 'a64_sgemm_8x12'
        }
    return simds[val]
# def conv_kernel(val: str):
#     d = {
#          "a64_sgemm_8x12": "sg8x12",
#          "a64_sgemm_8x6": "sg8x6",
#          "a64_hybrid_fp32_mla_4x24":"mla4x24",
#          "a64_hybrid_fp32_mla_8x4":"mla8x4",
#          "a64_hybrid_fp32_mla_6x16":"mla6x16",
#          }

