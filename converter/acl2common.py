import os
import numpy as np
import pandas as pd
from .common import Module
from .utils import *

class ACLModule(Module):
    def convert_layer_num(self, dir: str) -> np.array:
        l = os.listdir(dir)
        l = list(map(lambda x: x.split('.')[0], l))
        l = list(map(lambda x: int(x), l))
        l = list(set(l))
        l.sort()
        return np.array(l)
    
    def getlist(self, log_dir: str) -> [str]:
        l = os.listdir(log_dir)
        return l
    
    def __convert_acl2opt_per_one(self, csv_file: str) -> pd.DataFrame:
        p = pd.read_csv(csv_file)
        p = p.rename(columns={
            "cluster0_worksize.1": "cluster1_worksize"
            }, inplace=False)
        
        # if kernel compute time havent value in pandas data.
        # core6 data == kernel_compute_time.
        if p[['kernel_compute_time']].isnull().values.any():
            p = p.drop(labels=['kernel_compute_time'], axis=1)
            # Core7 is prepost in edger device
            p['kernel_compute_time'] = p["core6"] + p["core7"].fillna(0)
            # print(p['kernel_compute_time'])

        # print(selected_data)
        p["total_worksize"] = p["cluster0_worksize"] + p["cluster1_worksize"]
        p['info'] = p.apply(create_info_for_acl, axis=1)

        if 'prepost' in p.columns.to_list():
            p['prepost'] = p['prepost'].fillna(0)
            p['kernel_compute_time'] = p['kernel_compute_time'] + p['prepost']

        selected_data = p[['layer_id',
                           "cluster0_worksize",
                           "cluster1_worksize",
                           "kernel_compute_time",
                           'layer_name', 
                           'kernel_name',
                           'conv_alg',
                           'conv_simd',
                           'info',
                           'total_worksize']]
        
        selected_data = selected_data.drop_duplicates(subset=['cluster0_worksize', 'cluster1_worksize','conv_alg','conv_simd'])
        selected_data = selected_data.sort_values(by='cluster0_worksize').reset_index()
        return selected_data

    def __convert_acl2opt_per_layer(self, dir: str, layer_id: int):
        result = {}
        
        l = list(filter(lambda x: x.startswith(str(layer_id)), os.listdir(dir)))
        l = list(map(lambda x: os.path.join(dir, x), l))

        csv_files = list(map(lambda x: self.__convert_acl2opt_per_one(x), l))
        # print(csv_files)
        result['opt'] = csv_files
        # print(logs, len(fname), str(layer_id), fname)

        return csv_files

    def validate_layer(self, dir_list: [str]) -> bool:
        return True

    # def getlist(self, log_dir: str) -> [str]:
    #     l = os.listdir(log_dir)
    #     l = list(map(lambda x: os.path.join(log_dir, x), l))
    #     return l

    def convert(self, dir: str):
        # d = self.getlist(dir)
        # self.validate_layer(d)
        # assert(len(d) != 0)

        layers = self.convert_layer_num(dir)
        result = {}
        for layer in layers:
            result[layer] = {'opt': self.__convert_acl2opt_per_layer(dir, layer)}
        return result