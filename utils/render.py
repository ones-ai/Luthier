import matplotlib.pyplot as plt
import numpy as np

def get_each_min(data, index: int):
    spec = 0

    for key in data.keys():
        # kernels in layer
        item = data[key]
        ind = min(len(item['y']) - 1, index)
        
        spec += item['y'][ind]

    return spec

def get_min(result, index: int):
    keys = result.keys()
    assert(len(keys) != 0)

    m = None
    for key in keys:
        d = get_each_min(result[key], index)
        if m == None or d < m:
            m = d
    return m

def get_all_avg_line(result, opt: str, index: int):
    total = 0
    for indx in result:
        total += get_min(indx[opt], index)
    return total / len(result)


def get_all_min_line(result, opt: str, index: int):
    m = None
    for indx in result:
        value = get_min(indx[opt], index)
        if m == None or value < m:
            m = value
    return m

def get_all_max_line(result, opt: str, index: int):
    m = None
    for indx in result:
        value = get_min(indx[opt], index)
        if m == None or value > m:
            m = value
    return m

def make_charts_data(result, opt: str, max_iteration: int):
    max_line = []
    min_line = []
    avg_line = []

    for i in range(max_iteration):
        max_line.append(get_all_max_line(result, opt, i))
        min_line.append(get_all_min_line(result, opt, i))
        avg_line.append(get_all_avg_line(result, opt, i))
    
    return {'max': max_line, 'min': min_line, 'avg': avg_line}

def get_min_max(data, max_iteration: int, slice: int = 10):
    dmin, dmax = 99999999999, -1
    for item in list(data[0].keys()):
        datap = make_charts_data(data, item, max_iteration)
        tdmax = max(datap['avg'][slice:])
        dmax = max([dmax, tdmax])

        tdmin = min(datap['avg'][slice:])
        dmin = min([tdmin, dmin])
    return dmin, dmax

def render_data(chart_data, ax, name: str, index: int, show_legend: bool):
    c = chart_color_set(name)
    if not show_legend:
        name = None
        
    idx = np.arange(len(chart_data['avg']))[index:] + 1
    avg_data = chart_data['avg'][index:]
    max_data = chart_data['max'][index:]
    min_data = chart_data['min'][index:]
    
    ax.plot(idx, avg_data, color=c['line']['c'], linewidth=2, label=name, alpha=c['line']['a'])
    # plt.fill_between(
    #     idx, 
    #     max_data, min_data, color=c['back']['c'], alpha=c['back']['a']
    # )

def chart_color_set(name: str):
    colors = {
        'bayesian': {'line': {'c': 'orange', 'a': 1}, 
                'back': {'c': 'darkorange', 'a': 0.3}},
        "random": {'line': {'c': 'blue', 'a': 1}, 
                'back': {'c': 'lightblue', 'a': 0.7}},
        "xgboost": {'line': {'c': 'red', 'a': 1}, 
                'back': {'c': 'red', 'a': 0.3}},
        "grid": {'line': {'c': 'green', 'a': 1}, 
                'back': {'c': 'lightgreen', 'a': 0.3}},
        "greedy": {'line': {'c': 'purple', 'a': 1},
                'back': {'c': 'lavender', 'a': 0.3}}
    }

    return colors[name.lower()]

def render_figure(figsize: (float, float),
                  y_location: (float, float), 
                  x_location: (float, float),
                  title: str,
                  ):
    
    fig, ax = plt.subplots(2, 1, figsize=figsize)
    fig.supylabel("Laytency(ms)")
    fig.supxlabel("Samples", y=0.095)
    fig.suptitle("ResNet's Conv2")
    return fig, ax

def render_plt(result, ax, max_iteration: int, start_index: int = 10, show_legend: bool = False):
    assert(len(result) != 0)
    opt = list(result[0].keys())

    # f, ax = plt.subplots(figsize=(18, 6))

    for data in opt:
        d = make_charts_data(result, data, max_iteration)
        render_data(d, ax, data, start_index, show_legend)
    
    ax.grid(True)
    ax.tick_params(axis="y", length=0)
    
    mi, ma = get_min_max(result, max_iteration, start_index)
    r = int(max_iteration * 0.03)
    range = (ma - mi) / 10
    ax.set_ylim(mi - range, ma + range)
    ax.set_xlim(int(start_index - r), int(max_iteration + r))

    # plt.ylim(9, 14.5)
    # nyada
    # name of axis, label, title
    # x, y axis set detail name.
    # for label in ax.get_xticklabels() + ax.get_yticklabels():
    #     label.set_fontsize(18)
    #     label.set_bbox(dict(facecolor="white", edgecolor="None", alpha=0.65))

    # plt.savefig(save_path)
