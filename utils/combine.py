from .render import *

import pickle
import matplotlib.pyplot as plt
import numpy as np

def load_pickle(file):
    data = None

    with open(file,"rb") as fw:
        data = pickle.load(fw)

    assert(data != None)

    # [iter, args.save_figure, args.title_figure, args.tune_iteration]
    return [data[0], data[2], data[3]]

def combine_draw(pickles, title: str, output: str):
    data = list(map(lambda x: load_pickle(x), pickles))
    # Generate sample data for both graphs
    # Create the figure and subplots

    fig, axes = plt.subplots(len(pickles), 1, figsize=(10, 3 * len(pickles)))
    
    fig.suptitle(title, position=(0.5, 0.93), fontsize=20)
    fig.supylabel("Laytency(ms)", x=0.06, fontsize=18)
    fig.supxlabel("Samples", y=0.01, fontsize=18)

    assert(len(data) == len(axes))
    
    max_optimizer_count = 0
    for idx in range(len(data)):
        ax = axes[idx]
        iter, title, iteration = data[idx][0], data[idx][1], data[idx][2]

        # Plot data for EdgeR graph
        render_plt(iter, ax, iteration, 10, idx == 0)
        ax.set_title(title, x=0.5, y=0.8, fontsize=16)
        ax.tick_params(axis='both', which='major', labelsize=15)
        max_optimizer_count = len(list(iter[0].keys()))
        # # Plot data for Odroid N2+ graph

    # fig.tight_layout()
    fig.legend(fontsize=14, loc=(0.735, 0.85 - (max_optimizer_count * 0.045)))
    fig.savefig(output)

    # m3 = make_charts_data(iter1, "xgboost", 200)
    # m4 = make_charts_data(iter2, "xgboost", 200)6
    # print(m3['avg'][-1])
    # print(m4['avg'][-1])

    # module = ACLModule()
    # m1 = module.convert('./log/acl_resnet18_on_odroid')
    # m2 = module.convert('./log/acl_resnet18_on_edger')
    # # %%
    # print(min(m1[259]['opt'][0]['kernel_compute_time'])/ 1000 / 1000)
    # print(min(m2[259]['opt'][0]['kernel_compute_time'])/ 1000 / 1000)
#%%

# %%
