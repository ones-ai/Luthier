import argparse
from converter import ACLModule, XNNPackModule, Module
from optimizer import BayesianOptimizer, XGBoostOptimizer, RandomOptimizer, Optimizer, GridOptimizer, GreedyOptimizer
import random as rd

def srt_to_backend(argument_value) -> Module:
    if argument_value == 'acl':
        return ACLModule()
    elif argument_value == 'xnnpack':
        return XNNPackModule()
    else:
        raise argparse.ArgumentError(argument_value, "Invalid choice: 'acl' or 'xnnpack' expected")

def str_to_optimizer(argument_value) -> [Optimizer]:
    opt = argument_value.split(',')
  
    filters = {'random': RandomOptimizer(), 
               'bayesian': BayesianOptimizer(), 
               'xgboost': XGBoostOptimizer(), 
               'grid': GridOptimizer(), 
               'greedy': GreedyOptimizer()
               }
    
    result = []
    for item in opt:
        if item.strip() in filters:
            result.append(filters[item])
        else:
            continue

    if len(result) == 0:
        raise argparse.ArgumentError(argument_value, "Invalid choice: 'random', 'bayesian', 'xgboost', 'grid', 'greedy' expected")
    
    return result

def str_to_random_seed(arg) -> [int]:
    data = arg.split(',')
    
    try:
        return list(map(lambda x: int(x), data))
    except ValueError:
        raise argparse.ArgumentError(arg, "Invalid choice: '10,20,30' expected")

def parse_argument():
    parser = argparse.ArgumentParser(description='Luthier')
    parser.add_argument('-s', '--show_list',
                    action='store_true')  # on/off flag
    parser.add_argument('-p', '--save_pickle',
                    action='store_true')  # on/off flag
    parser.add_argument('-f', '--save_figure',
                    action='store_true')  # on/off flag
    
    parser.add_argument("--backend", type=srt_to_backend,
                    help="Select backend (acl or xnnpack)", required=True)
    parser.add_argument("--directory", type=str,
                    help="Path of log data  to the directory to read", required=True)
    
    parser.add_argument("--random_seed_xgboost", type=str_to_random_seed,
                    help="random seed for xgboost, if random seed is not entered, select random seed in python.", default=None)
    parser.add_argument("--random_seed_bayesian", type=str_to_random_seed,
                    help="random seed for bayesian, if random seed is not entered, select random seed in python.", default=None)
    parser.add_argument("--random_seed_random", type=str_to_random_seed,
                    help="random seed for random optimizer, if random seed is not entered, select random seed in python.", default=None)
    # parser.add_argument("--random_seed_grid", type=str_to_random_seed,
    #                 help="random seed for grid optimizer, if random seed is not entered, select random seed in python.", default=None)
    parser.add_argument("--random_seed_greedy", type=str_to_random_seed,
                    help="random seed for greedy optimizer, if random seed is not entered, select random seed in python.", default=None)
    
    parser.add_argument("--optimizer", type=str_to_optimizer,
                    help="Select optimizer (random, bayesian, xgboost), split by ','", default=RandomOptimizer())
    parser.add_argument("--tune_iteration", type=int,
                    help="The number of iterations of optimization", default=500)
    parser.add_argument("--search_iteration", type=int,
                    help="The number of iterations of repeatation of tune_iteration", default=1)
    parser.add_argument("--layer_id", type=int,
                    help="layer id in log")
    parser.add_argument("--title_figure", type=str,
                    help="set figure title", default='Result')
    
    parser.add_argument("--device_name", type=str,
                    help="set device name", default="(Null)")
    parser.add_argument("--save_output", type=str,
                    help="to save location of figure", default='./result')
    
    args = parser.parse_args()

    # validate
    a = args.search_iteration if args.random_seed_xgboost == None else len(args.random_seed_xgboost)
    b = args.search_iteration if args.random_seed_bayesian == None else len(args.random_seed_bayesian)
    c = args.search_iteration if args.random_seed_random == None else len(args.random_seed_random)
    # d = args.search_iteration if args.random_seed_grid == None else len(args.random_seed_grid)
    e = args.search_iteration if args.random_seed_greedy == None else len(args.random_seed_greedy)
    
    if not (a >= args.search_iteration and 
            b >= args.search_iteration and 
            c >= args.search_iteration and
            # d >= args.search_iteration and
            e >= args.search_iteration):
        raise argparse.ArgumentError("", "Invalid value 'random seed values(random_seed_*~, search_iteration) is not equal'")

    return args
