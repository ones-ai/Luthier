FROM python:3.9-bullseye AS builder

WORKDIR /app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Demo: Online Download 
RUN wget https://gitlab.com/ones-ai/Luthier/uploads/0a7b3f3650f991f43f107b98f51a3723/log.tar.gz && \
    tar -zxf ./log.tar.gz && \
    rm -rf ./log.tar.gz

COPY . .
