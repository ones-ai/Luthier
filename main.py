from utils import *
import numpy as np
import pandas as pd
import random as rd

import logging
import pickle

logging.basicConfig(format='[main] %(levelname)s : %(message)s ', level=logging.DEBUG)

def get_random_seed(array, index: int) -> int:
    if array != None:
        seed = array[index]
    else:
        seed = rd.randint(0, 2 ** 32 - 1) # uint.max
    return seed

def get_seed(args, optiizer_name: str, index: int) -> int:
    if optiizer_name == "Bayesian":
        return get_random_seed(args.random_seed_bayesian, index)
    elif optiizer_name == "XGBoost":
        return get_random_seed(args.random_seed_xgboost, index)
    elif optiizer_name == "Random":
        return get_random_seed(args.random_seed_random, index)
    elif optiizer_name == "Grid":
        return get_random_seed(None, index)
    elif optiizer_name == "Greedy":
        return get_random_seed(args.random_seed_greedy, index)
    else:
        raise "Error, your random seed is not correct. INFO : [{}, {}]".format(optiizer_name, index)

if __name__ == "__main__":
    args = parse_argument()
    if args.show_list:
        print("Select: Below Layer Id")
        print(args.backend.convert_layer_num(args.directory))
        exit()
    logging.info('Start. from Log Data to Format for Optimizer')
    data = args.backend.convert(args.directory)
    logging.info('End. from Log Data to Format for Optimizer')

    iter = []
    for seach_iter in range(args.search_iteration):
        result = {}
        for opt in args.optimizer:
            # search for all kernel 
            layer_id = args.layer_id
            algs = data[layer_id].keys()
            result[opt.name()] = {}
            for alg in algs:
                result[opt.name()][alg] = {}
                for ind in range(len(data[layer_id][alg])):
                    k = data[layer_id][alg][ind]
                    opt.init(k)
                    seed = get_seed(args, opt.name(), seach_iter)
                    opt.train(args.tune_iteration, seed)
                    opt_result = opt.result()
                    result[opt.name()][alg][ind] = opt_result
        iter.append(result)

    if args.save_pickle:
        with open("{0}.pickle".format(args.save_output),"wb") as fw:
            pickle.dump([iter, args.save_output, args.device_name, args.tune_iteration], fw)

    if args.save_figure:
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.set_title(args.title_figure)
        ax.set_ylabel("Laytency(ms)")
        ax.set_xlabel("Samples")
        render_plt(iter, ax, args.tune_iteration, 0, True)
        fig.legend(loc=(0.77, 0.68))
        fig.savefig(args.save_output + ".pdf")
