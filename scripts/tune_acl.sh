#!/bin/bash

xgboost="$RANDOM,$RANDOM,$RANDOM,$RANDOM,$RANDOM"
random="$RANDOM,$RANDOM,$RANDOM,$RANDOM,$RANDOM"
bayesian="$RANDOM,$RANDOM,$RANDOM,$RANDOM,$RANDOM"
greedy="$RANDOM,$RANDOM,$RANDOM,$RANDOM,$RANDOM"

python3 ./main.py --backend acl \
                  --optimizer xgboost,bayesian,greedy,random \
                  --directory ./log/acl_resnet18_on_edger \
                  --tune_iteration 200 \
                  --search_iteration 5 \
                  --layer_id 259 \
                  --device_name "EdgeR" \
                  --title_figure "ResNet's Conv2" \
                  --save_output ./acl_resnet18_conv2 \
                  --save_figure \
                  --random_seed_xgboost ${xgboost} \
                  --random_seed_bayesian ${random} \
                  --random_seed_greedy ${bayesian} \
                  --random_seed_random ${greedy}
