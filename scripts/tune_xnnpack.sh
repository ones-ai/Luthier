#!/bin/bash

xgboost="$RANDOM,$RANDOM,$RANDOM,$RANDOM,$RANDOM"
random="$RANDOM,$RANDOM,$RANDOM,$RANDOM,$RANDOM"
bayesian="$RANDOM,$RANDOM,$RANDOM,$RANDOM,$RANDOM"
greedy="$RANDOM,$RANDOM,$RANDOM,$RANDOM,$RANDOM"

python3 ./main.py --backend xnnpack \
                  --optimizer xgboost,bayesian,greedy,random \
                  --directory ./log/xnnpack_resnet18_on_odroid \
                  --tune_iteration 200 \
                  --search_iteration 5 \
                  --layer_id 1 \
                  --device_name "Odroid N2+" \
                  --title_figure "ResNet's Conv1" \
                  --save_output ./xnnpack_resnet18_conv1 \
                  --save_figure \
                  --random_seed_xgboost ${xgboost} \
                  --random_seed_bayesian ${random} \
                  --random_seed_greedy ${bayesian} \
                  --random_seed_random ${greedy}
