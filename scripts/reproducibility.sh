#!/bin/bash

mkdir output

current_datetime=$(date +"%Y-%m-%d %H:%M:%S")

edger_xgboost="12595,8634,16524,13362,29481"
edger_random="25961,24220,13568,7414,20547"
edger_bayesian="10647,23328,6914,8427,14189"
edger_greedy="32430,23457,10014,1749,5804"
edger_file_name="${edger_xgboost}.${edger_random}.${edger_bayesian}.${edger_greedy}"

odroid_xgboost="5349,21502,7159,5778,25578"
odroid_random="2080,1197,22066,17378,4648"
odroid_bayesian="628,31363,27772,6876,22483"
odroid_greedy="29519,13843,16053,1630,27291"
odroid_file_name="${odroid_xgboost}.${odroid_random}.${odroid_bayesian}.${odroid_greedy}"

python3 ./main.py --backend acl \
                    --optimizer xgboost,bayesian,greedy,random \
                    --directory ./log/acl_resnet18_on_edger \
                    --tune_iteration 200 \
                    --search_iteration 5 \
                    --layer_id 259 \
                    --device_name "EdgeR" \
                    --title_figure "ResNet's Conv2" \
                    --save_output "./output/edger_acl_resnet18_conv2 - ${current_datetime}.${edger_file_name}" \
                    --save_pickle \
                    --random_seed_xgboost ${edger_xgboost} \
                    --random_seed_bayesian ${edger_bayesian} \
                    --random_seed_random ${edger_random} \
                    --random_seed_greedy ${edger_greedy}

python3 ./main.py --backend acl \
                    --optimizer xgboost,bayesian,greedy,random \
                    --directory ./log/acl_resnet18_on_odroid \
                    --tune_iteration 200 \
                    --search_iteration 5 \
                    --layer_id 259 \
                    --device_name "Odroid N2+" \
                    --title_figure "ResNet's Conv2" \
                    --save_output "./output/odroid_acl_resnet18_conv2 - ${current_datetime}.${odroid_file_name}" \
                    --save_pickle \
                    --random_seed_xgboost ${odroid_xgboost} \
                    --random_seed_bayesian ${odroid_bayesian} \
                    --random_seed_random ${odroid_random} \
                    --random_seed_greedy ${odroid_greedy}

python3 ./integrate.py --pickles "./output/edger_acl_resnet18_conv2 - ${current_datetime}.${edger_file_name}.pickle" \
                                 "./output/odroid_acl_resnet18_conv2 - ${current_datetime}.${odroid_file_name}.pickle" \
                        --title "ResNet's Conv2" \
                        --output "./output/result - ${current_datetime}.pdf"
